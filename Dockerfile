FROM node as builder

COPY src src
COPY package.json package.json
COPY tsconfig.json tsconfig.json
COPY package-lock.json package-lock.json

RUN npm install

RUN npm run build

FROM node:alpine as final

WORKDIR /app

COPY --from=builder ./dist ./

ENV NODE_ENV production

ENV TARGET "http://responder/respond"
ENV INTERVAL 1000

CMD ["node", "index.js"]

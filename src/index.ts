import {LogEntry} from "./utils/logger/LogEntry";

const target = process.env.TARGET;
const interval = parseInt(process.env.INTERVAL ?? '1000');

function handler() {
    if (!target) {
        console.error('No Target provided. I don\'t know where to ping :(');
        return;
    }

    const logEntry = new LogEntry();

    console.log(`Sending request to: ${target}`);
    logEntry.setRequestReceived();
    fetch(target)
        .then((response) => {
            return logEntry.fromResponse(response)
        })
        .then(() => {
            console.log(`Received response from ${target}`);
            console.log('Logging String: ' + logEntry.toString());
        })
        .catch((error) => {
            console.error(error);
        });
}


console.table({
    target,
    interval,
});

setInterval(handler, interval);
handler();

process.on('SIGINT', function() {
    console.log("Caught interrupt signal, exiting...");
    process.exit();
});

process.on('SIGTERM', function() {
    console.log("Caught interrupt signal, exiting...");
    process.exit();
});

